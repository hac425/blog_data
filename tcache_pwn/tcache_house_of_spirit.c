#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>




int main(int argc, const char* argv[]) {
    size_t fake_chunk_and_more[64];
    memset(fake_chunk_and_more, 'A', sizeof(fake_chunk_and_more));
    printf("stack buf: %p\n", (void *)fake_chunk_and_more);
    
    char* fake_chunk = (char * )fake_chunk_and_more;
    *(long *)(fake_chunk + sizeof(long)) = 0x110;

    *(long *)(fake_chunk + 0x110 + sizeof(long)) = 0x40;  // 设置 pre_inused 位
    char *mem = fake_chunk + 2*sizeof(long);
    free(mem);
    
    void *mem2 = malloc(0x100);
    printf("malloc(0x100) returned: %p\n", mem2);

    return 0;
}

